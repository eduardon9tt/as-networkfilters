# AS-NetworkFilters
Automated Scripts to set Secure Rules & Tips for Linux based Distros using Shell/Bash.

### Project Version: 2.1

  - Add sysctl.conf variables.
 Added file 01-harden.conf with variables for /etc/sysctl.conf. The variables was created for operation systems with 4GB RAM or more. This file is not recommended for use in servers.

  - Add save function
 If choose save will be default saved into /etc/iptables/ directory files with name "nf_rules.v4" and "nf_rules.v6".

## License
Released under MIT License
